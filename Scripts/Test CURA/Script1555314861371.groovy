import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')
WebUI.navigateToUrl('https://katalon-demo-cura.herokuapp.com/')
WebUI.click(findTestObject('CURA_Healthcare_Service/Page_Homepage/button_Make Appointment'))

WebUI.setText(findTestObject('CURA_Healthcare_Service/Page_Login/input_Username_username'), 'John Doe')
WebUI.setEncryptedText(findTestObject('CURA_Healthcare_Service/Page_Login/input_Password_password'), 
    'g3/DOGG74jC3Flrr3yH+3D/yKbOqqUNM')
WebUI.click(findTestObject('CURA_Healthcare_Service/Page_Login/button_Login'))
WebUI.selectOptionByValue(findTestObject('CURA_Healthcare_Service/Page_Make_Appointment/select_Facility'), 
    'Hongkong CURA Healthcare Center', true)
WebUI.click(findTestObject('CURA_Healthcare_Service/Page_Make_Appointment/checkbox_Apply for hospital readmission_hospital_readmission'))
WebUI.click(findTestObject('CURA_Healthcare_Service/Page_Make_Appointment/input_Medicare_programs'))
WebUI.setText(findTestObject('CURA_Healthcare_Service/Page_Make_Appointment/input_visit_date'), 
    '15/04/2019')
WebUI.setText(findTestObject('CURA_Healthcare_Service/Page_Make_Appointment/textarea_comment'), 
    'My first appointment')

WebUI.click(findTestObject('CURA_Healthcare_Service/Page_Make_Appointment/button_Book Appointment'))

WebUI.verifyElementText(findTestObject('CURA_Healthcare_Service/Page_Appointment_Confirmation/label_facility'), 'Hongkong CURA Healthcare Center')
WebUI.verifyElementText(findTestObject('CURA_Healthcare_Service/Page_Appointment_Confirmation/label_readmission'), 'Yes')
WebUI.verifyElementText(findTestObject('CURA_Healthcare_Service/Page_Appointment_Confirmation/label_program'), 'Medicare')
WebUI.verifyElementText(findTestObject('CURA_Healthcare_Service/Page_Appointment_Confirmation/label_visit_date'), '15/04/2019')
WebUI.verifyElementText(findTestObject('CURA_Healthcare_Service/Page_Appointment_Confirmation/label_comment'), 'My first appointment')

WebUI.click(findTestObject('CURA_Healthcare_Service/Page_Appointment_Confirmation/a_Go to Homepage'))

